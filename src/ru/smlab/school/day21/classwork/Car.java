package ru.smlab.school.day21.classwork;

public class Car implements Comparable {
    String model;
    String color;

    public Car(String model, String color) {
        this.model = model;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public int hashCode(){
        return 1;
    }

    public boolean equals(Object o) {
        Car another = (Car) o;
        return this.color.equals(another.color) && this.model.equals(another.model);
    }

    @Override
    public int compareTo(Object o) {
        return this.color.compareTo(((Car) o).color);
    }
}
