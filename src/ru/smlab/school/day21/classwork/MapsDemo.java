package ru.smlab.school.day21.classwork;

import java.util.*;

public class MapsDemo {
    public static void main(String[] args) {

        Map<String, Car> maps = new HashMap<>();


        String key = "А456ВТ779";
        System.out.println(key.hashCode());
        System.out.println(Math.abs(key.hashCode() % 16));

        //add Set List
        maps.put("А456ВТ770", new Car("Kia Ceed", "blue"));
        maps.put("А456ВТ779", new Car("Kia Ceed", "blue"));
        maps.put("Н945АК50", new Car("Kia Rio", "black"));
        maps.put("М221НВ91", new Car("Nissan Almera", "blue"));
        maps.put("В456УЕ54", new Car("Opel Mokka", "white"));
//        maps.put(null, new Car("Opel Mokka", "red"));
//        maps.put(null, new Car("Opel Mokka", "red11"));

        System.out.println(maps);

        for(Map.Entry<String, Car> entry : maps.entrySet()){
            System.out.println(entry);
        }

//        Car c = maps.get("М221НВ91");
//        System.out.println(c);
//        Car c1 = maps.get("М251НВ95");
//        System.out.println(c1);
////        Car c2 = maps.get(null);
////        System.out.println(c2);
//
//        TreeMap<Car, String> cars = new TreeMap<>();
//        Car c = new Car("Chevrolet", "lime");
//        cars.put(new Car("Chevrolet", "black"), "VIN number 54536346436346");
//        cars.put(new Car("Chevrolet", "green"), "VIN number 5453634649999999996");
//        cars.put(c, "VIN number 5453634649999999996");
//        cars.put(new Car("Chevrolet", "blue"), "VIN number 5453634649999999996");
//
//        System.out.println("5%%% "+cars.tailMap(c));
//
//        System.out.println(cars);



    }
}
