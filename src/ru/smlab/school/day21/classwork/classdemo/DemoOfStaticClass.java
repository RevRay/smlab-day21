package ru.smlab.school.day21.classwork.classdemo;

public class DemoOfStaticClass {
    int i = 0;

    static class StaticNestedClass {

    }

    public static void main(String[] args) {
        StaticNestedClass s = new StaticNestedClass();
    }
}
